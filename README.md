# Threaded Mod Example

A starting point for a mod that needs to differentiate between main thread, map threads, and common code easier.

## Documentation
main.js will be run on the main thread.
map.js will be run on the map thread.
Both map and main will be extended on to common.js, so anything in common will be in the threads too.
Map and main have init functions which are called on startup, and common has commonInit for anything that needs to be in both.

There is also a special require function which is part of all threads.
You can call it with this.require(path, isLocal).
If isLocal is false or undefined, it requires from src/server (so for example, this.require('misc/events') would be src/server/misc/events).
If isLocal is true, it requires from the mod folder (example: this.require('threads/common', true) requires src/server/mods/threaded-mod-example/threads/common, except replacing the name of the mod with the correct folder).
